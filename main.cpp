#include <iostream>
#include <dirent.h>
#include <fstream>
#include <sys/stat.h>
#include "ImageCategory.h"

#define BINS 64

int getcategories(std::string, std::vector<ImageCategory>&);
void writeARFF(std::string, std::vector<ImageCategory>&);

int main(int argc, char** argv) {
    char arff_name[24];
    sprintf(arff_name, "test%d.arff", BINS);
    const std::string root_dir = std::getenv("HOME") + std::string("/Desktop/project-images/");
    const std::string arff_path = root_dir + arff_name;

    Image::setNumBins(BINS);

    // Delegate image processing tasks to category objects
    std::vector<ImageCategory> cats;
    getcategories(root_dir, cats);
    for (int i = 0; i < cats.size(); ++i) {
        cats[i].getImages();
        cats[i].getRGBdata();
    }

    // Compose ARFF file
    writeARFF(arff_path, cats);

    return 0;
}

void writeARFF(std::string path, std::vector<ImageCategory>& cats) {
    // Open file stream
    std::ofstream arff;
    arff.open(path);

    // relation type
    arff << "@relation NormalizedColorHistogram_" << BINS << "Bins";
    arff << std::endl << std::endl;

    // attributes
    for (int i = 0; i < BINS; ++i)
        arff << "@attribute bin" << i + 1 << " numeric" << std::endl;
    arff << "@attribute category {piano, kangaroo, strawberry, sunflower, airplane, face, leopard}";
    arff << std::endl << std::endl;

    // data
    arff << "@data" << std::endl;
    for (int i = 0; i < cats.size(); ++i)
        arff << cats[i].getARFFStrings();

    arff.close(); // close file stream
}

int getcategories(std::string path, std::vector<ImageCategory>& cats) {
    struct dirent *ent;
    DIR *dp = opendir(path.c_str());

    if (dp == NULL)
        return -1;

    // Load categories from folder names in project-image directory
    while ((ent = readdir(dp))) {
        std::string name = ent->d_name;
        struct stat st;
        lstat((path + name).c_str(), &st);

        // Make sure current entry is a directory
        if(S_ISDIR(st.st_mode) && name[0] != '.') {
            cats.push_back(ImageCategory(name, path));
        }
    }

    closedir(dp);
    return  0;
}