#include "ImageCategory.h"

ImageCategory::ImageCategory(std::string name, std::string root) : name(name) {
    path = root;
    if (root.back() != '/')
        path += '/';
    path += name + "/training/";
}

void ImageCategory::getImages() {
    struct dirent *entry;
    DIR *dp = opendir(path.c_str());

    while (dp != NULL && (entry = readdir(dp))) {
        std::string name = entry->d_name;
        std::string ext  = name.substr(name.find('.'), 4);

        // If current directory object is a .jpg image, add to vector
        if (ext == ".jpg") {
            Image next(name, path, this->name);
            images.push_back(next);
        }
    }

    closedir(dp);
}

// Delegate pixelwise processing to image objects
void ImageCategory::getRGBdata() {
    for (int i = 0; i < images.size(); ++i) {
        images[i].getColorFrequencies();
    }
}

// return ARFF strings from images to caller to compose ARFF file
std::string ImageCategory::getARFFStrings() {
    std::ostringstream oss;
    for (int i = 0; i < images.size(); ++i)
        oss << images[i].getARFFString() << std::endl;
    return oss.str();
}