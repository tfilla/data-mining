/** Mining -- ImageCategory.h
 * Created by Aquilla Sherrock on 01 December, 2014.
 * Copyright (c) 2014 Insignificant Tech. All rights reserved.
 */

#ifndef __ImageCategory_H_
#define __ImageCategory_H_

#include <sstream>
#include <iostream>
#include <dirent.h>
#include "Image.h"

class ImageCategory {
private:
    std::vector<Image> images;
public:
    std::string name;
    std::string path;

    ImageCategory(std::string name, std::string root);

    void getImages();
    void getRGBdata();
    std::string getARFFStrings();
};

#endif /* defined __ImageCategory_H_ */